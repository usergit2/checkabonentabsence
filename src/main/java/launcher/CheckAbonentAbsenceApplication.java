package launcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckAbonentAbsenceApplication {

    public static void main(String[] args){
        SpringApplication.run(CheckAbonentAbsenceApplication.class, args);
    }
}
