package utils;

import com.google.gson.Gson;

public class Utils {

    public static <T> T convertJSONtoObject(String json, Class<T> t){
        Gson gson = new Gson();
        T t1  = gson.fromJson(json, t);
        return t1;
    }

    public static String convertObjectToJSON(Object t){
        Gson gson = new Gson();
        return gson.toJson(t);
    }
}
