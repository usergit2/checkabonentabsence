package controller;

import json.objects.UnavailableSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import workflows.CheckAbonentWorkflow;


@Controller
@EnableAutoConfiguration
public class RestController {

    private static Logger log = LoggerFactory.getLogger(RestController.class);

    @RequestMapping(value = "${unavailableSubscriber.host}", method = RequestMethod.POST)
    public void notifySubscriber(@RequestBody UnavailableSubscriber unavailableSubscriber) {
        try {
            CheckAbonentWorkflow.getSubscriberPingResponse(unavailableSubscriber);
        }catch (Exception e){
            log.debug(e.getMessage());
        }
    }
}
