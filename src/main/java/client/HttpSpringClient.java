package client;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class HttpSpringClient {

    private static Logger log = LoggerFactory.getLogger(HttpSpringClient.class);

    public static  <T> ResponseEntity<T> sendHttpRequest(String url, HttpMethod httpMethod,
                                                         HttpEntity entity, Class<T> t) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<T> response = null;
        try {
            response = restTemplate.exchange(url, httpMethod, entity, t);
            log.debug("HTTP ответ: " + response.getBody());
        } catch (HttpClientErrorException e){
            log.debug("Запрос не был отправлен либо ответ не был получен");
        }
        return response;
    }
}
