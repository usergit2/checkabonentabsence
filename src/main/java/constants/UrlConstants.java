package constants;

public final class UrlConstants {

    public static final String UNAVAILABLE_SUBSCRIBER_HOST = System.getProperty("unavailableSubscriber.host");
    public static final String EXTERNAL_URL_HOST = System.getProperty("external-url.host");
    public static final String SMS_SERVICE_HOST = System.getProperty("sms-service.host");
}
