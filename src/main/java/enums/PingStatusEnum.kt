package enums

enum class PingStatusEnum {

    unavailableSubscriber, inNetwork
}