package json.objects;

public class UnavailableSubscriber {

    private int msisdnA;
    private int msisdnB;

    public int getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(int msisdnA) {
        this.msisdnA = msisdnA;
    }

    public int getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(int msisdnB) {
        this.msisdnB = msisdnB;
    }
}
