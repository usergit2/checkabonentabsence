package json.objects;

public class SendSmsJson {

    private int msisdnA;
    private int msisdnB;
    private String text;

    public int getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(int msisdnA) {
        this.msisdnA = msisdnA;
    }

    public int getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(int msisdnB) {
        this.msisdnB = msisdnB;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
