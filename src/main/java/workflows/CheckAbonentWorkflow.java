package workflows;

import client.HttpSpringClient;
import constants.StringConstants;
import constants.TimeConstants;
import constants.UrlConstants;
import enums.PingStatusEnum;
import json.objects.SendSmsJson;
import json.objects.SubscriberPingResponse;
import json.objects.UnavailableSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

public class CheckAbonentWorkflow {

    private static Logger log = LoggerFactory.getLogger(CheckAbonentWorkflow.class);

    public static void getSubscriberPingResponse(UnavailableSubscriber unavailableSubscriber) throws Exception {
        String url = UrlConstants.EXTERNAL_URL_HOST + unavailableSubscriber.getMsisdnB();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity entity = new HttpEntity(Utils.convertObjectToJSON(unavailableSubscriber), headers);
        log.debug("HTTP запрос = " + entity.toString());

        ResponseEntity<String> response = HttpSpringClient.sendHttpRequest(url, HttpMethod.GET, entity,
                String.class);
        if(response.getStatusCode()!= HttpStatus.OK){
            throw new Exception("Статус Http ответа не OK. " + response.getStatusCode());
        }
        SubscriberPingResponse subscriberPingResponse =  Utils.convertJSONtoObject(response.getBody(),
                SubscriberPingResponse.class);

        switch (PingStatusEnum.valueOf(subscriberPingResponse.getStatus())){

            case unavailableSubscriber:
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {

                    @Override
                    public void run() {}},
                        0, TimeConstants.PING_DELAY);
                getSubscriberPingResponse(unavailableSubscriber);break;
            case inNetwork:
                SendSmsJson json = new SendSmsJson();
                json.setMsisdnA(unavailableSubscriber.getMsisdnA());
                json.setMsisdnB(unavailableSubscriber.getMsisdnB());
                json.setText(StringConstants.SMS_TEXT);
                sendSMS(json);
                break;
            default:
                throw new IllegalStateException("Неожиданный статус HTTP ответа: " + subscriberPingResponse.getStatus());
        }
    }

    public static void sendSMS(SendSmsJson json) throws Exception {

        HttpHeaders headers = new HttpHeaders();
        HttpEntity entity = new HttpEntity(json,headers);
        log.debug("HTTP запрос = " + entity.toString());

        ResponseEntity<String> response = HttpSpringClient.sendHttpRequest(UrlConstants.SMS_SERVICE_HOST, HttpMethod.POST,
                entity, String.class);
        if(response.getStatusCode()!= HttpStatus.OK){
            throw new Exception("Статус Http ответа не OK. " + response.getStatusCode());
        }
    }
}
